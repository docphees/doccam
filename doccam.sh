# DocCam.sh
# v1.02
# Launching doccam.sh and creating a config file if not already present

# create a doccam.input.conf in ~/.config/mpv if not already present
if [ ! -f ~/.config/mpv/doccam.input.conf ]; then
echo "# input configuration for doccam usage

Ctrl+q quit

q add video-zoom -0.05
e add video-zoom 0.05
WHEEL_UP add video-zoom 0.05
WHEEL_DOWN add video-zoom -0.05

a add video-pan-x 0.01
d add video-pan-x -0.01
w add video-pan-y 0.01
s add video-pan-y -0.01
LEFT add video-pan-x 0.01
RIGHT add video-pan-x -0.01
UP add video-pan-y 0.01
DOWN add video-pan-y -0.01

BS set video-zoom 0 ; set video-pan-x 0 ; set video-pan-y 0

SPCE cycle pause; drop-buffers
ENTER playlist-next

ESC drop-buffers" >> ~/.config/mpv/doccam.input.conf
fi

# print a short help text
echo "USAGE of doccam.sh

q/e - zoom out/in
MOUSEWHEEL - zoom
w/a/s/d - pan the image up/left/down/right
Arrow keys - pan the image
ENTER - cycle through available cameras
Ctrl+q - exit doccam (mpv)
BACKSPACE - re-center image and zoom to 100%
ESC - drop buffers (reset video stream if lag is too high)
SPACE - freeze/resume the video stream"

# launching mpv with the input configuration
mpv /dev/video9 /dev/video8 /dev/video7 /dev/video6 /dev/video5 \
  /dev/video4 /dev/video3 /dev/video2 /dev/video1 /dev/video0 \
  --input-conf=~/.config/mpv/doccam.input.conf \
  --loop-playlist --speed=2 --fs
