# DocCam

A USB document camera presentation solution for Linux that simply works!

This is wrapped around the mpv media player, which has to be installed seperately.

Version 1.02

## Idea and Scope

I have a simple USB document camera (VIS 300), which has to be connected to a computer, which
in turn is connected to a projector. The camera is cheap and is powered through
the USB port itself, making it very portable.

*But:* There is no proper open/free software available for Linux to use it. The
camera itself is recognised as a USB camera, using built in v4l2 drivers and
works out of the box. Sure, *Cheese* or *guvcview* can be used, but are not really 
meant for presentations. My plan was, to build a simple software, which would work
for all USB cameras.

*The solution:* I found, that all software is already available in the Linux ecosystem:

I refactored the brilliant [mpv media player](https://mpv.io/) with a few lines of configuration items,
and the solution is maybe not the same as on windows (with screenshot tools etc.),
but fits more the unix way of thinking. Let me talk you through it:

You can put the launcher anywhere (doccam.sh) - I have it in my ~/bin/ directory.

A doccam.input.conf file will automatically be created in ~/.config/mpv/
You can easily add more commands. Use the input commands manual:
https://mpv.io/manual/master/#list-of-input-commands

## Usage
Simply download doccam.sh and put it somewhere, for example a ~/bin/ directory
(or /usr/bin for all users if you want).

Make sure, *mpv* is installed on your system (check your software center, it
should be there).

Now run doccam.sh and mpv will open, showing the video stream from an available
camera.

### Control

To make usage simple, the available keys are:
*  q/e - zoom out/in
*  MOUSEWHEEL - zoom
*  w/a/s/d - pan the image up/left/down/right
*  Arrow keys - pan the image
*  ENTER - cycle through available cameras
*  Ctrl+q - exit doccam (mpv)
*  BACKSPACE - re-center image and zoom to 100%
*  ESC - drop buffers (reset video stream if lag is too high)
*  SPACE - freeze/resume the video stream

## Dependencies, a.k.a. Which Cameras Work?

* Any camera, that shows up as /dev/video* should work! (It has to have a video for linux (v4l) driver.)
* YOU HAVE TO install mpv for this to work.

## I have more than one Camera

You can use c to cycle through all availble video devices. The launch script
basically creates a playlist with all devices from /dev/video9 down to /dev/video0.
If you have more than these 10 devices, quickly edit in a few more items.

Using that instead of adding all /dev/video* items allows to connect cameras while
doccam is already running.

## The initial doccam.input.conf

If there is no doccam.input.conf in ~/.config/mpv/, doccam will automatically 
create this file for you:
```
# input configuration for doccam usage

Ctrl+q quit

q add video-zoom -0.05
e add video-zoom 0.05
WHEEL_UP add video-zoom 0.05
WHEEL_DOWN add video-zoom -0.05

a add video-pan-x 0.01
d add video-pan-x -0.01
w add video-pan-y 0.01
s add video-pan-y -0.01
LEFT add video-pan-x 0.01
RIGHT add video-pan-x -0.01
UP add video-pan-y 0.01
DOWN add video-pan-y -0.01

BS set video-zoom 0 ; set video-pan-x 0 ; set video-pan-y 0

SPCE cycle pause; drop-buffers
ENTER playlist-next

ESC drop-buffers
```

Use the manual (https://mpv.io/manual/master/#list-of-input-commands) to 
add/edit the keybindings to fit your style:

* mpv supports its own screenshots
* add more control

## The Launch script

The launch script (doccam.sh) does three things:
* create the input configuration file if not yet present
* set up mpv for presentation mode
* create a playlist with your system's video devices

You can easily edit the launch script. For example:

* you don't want to run doccam/mpv in full screen
* you want to manually setup video streams to funny ways
* change the input config file location or name
* anything that's written here: https://mpv.io/manual/master/

## Questions & Answers

#### *Isn't this too primitive?*

The solution is as out-of-your-face as it can get. It is a one-click affair,
doing everything related to the stream (zoom/panning/pause/contrast/brightness/gamma) 
and accesses the
video devices as linux/unix/posix meant it.
Everything else (like screenshots) can be done with other, dedicated tools.

#### *But - this is just mpv*

Yep. That's the beauty. A bit of tweaking and it is done.

#### *I want to take screenshots*

I am using dedicated screenshot tools (the desktop environment's tool, 
bound to the "print" key) for that.
But mpv has built-in screenshot support. You can set up a key in the 
doccam.input.conf for that. (See more above under "The initial doccam.input.conf")

#### *Why do you start with the video9 device and go in reverse to video0?*

In my usage case, the external USB camera gets a higher number and I want to
get that video stream as default instead of my built-in laptop camera. Feel free
to change that in the doccam.sh file.

#### *I edited my doccam.input.conf, and it is now better than yours!*

Please share it with us. My version is a very simple first start. Anything more 
elaborate would help me and other users.

I would want to have panning by mouse - either clicking or dragging. I think it
might be possible with mpv's on-board capabilities.

#### *Why mpv? Why not [insert media player here]?*

I am using mpv. This should work with mplayer, and probably vlc, and probably 
many, many other media players. But I am using mpv.